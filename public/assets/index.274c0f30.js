import{_ as o,c as t,a as s,b as n,o as c}from"./index.d5e3151d.js";const l={class:"markdown-body"},a=n(`<h1>\u4ECB\u7ECD</h1><p>JwChat \u662F\u501F\u9274 <a href="https://avuejs.com/">AVue</a> \u3002<a href="https://element-plus.gitee.io/zh-CN/">element-plus</a> \u8FDB\u884C\u5F00\u53D1\u7684 <code>IM</code> \u7EC4\u4EF6\u3002</p><h3>\u6548\u7387 Efficiency</h3><ul><li><strong>\u7B80\u5316\u6D41\u7A0B\uFF1A</strong> \u8BBE\u8BA1\u7B80\u6D01\u76F4\u89C2\u7684\u64CD\u4F5C\u6D41\u7A0B\uFF1B</li><li><strong>\u6E05\u6670\u660E\u786E\uFF1A</strong> \u8BED\u8A00\u8868\u8FBE\u6E05\u6670\u4E14\u8868\u610F\u660E\u786E\uFF0C\u8BA9\u7528\u6237\u5FEB\u901F\u7406\u89E3\u8FDB\u800C\u4F5C\u51FA\u51B3\u7B56\uFF1B</li><li><strong>\u5E2E\u52A9\u7528\u6237\u8BC6\u522B\uFF1A</strong> \u754C\u9762\u7B80\u5355\u76F4\u767D\uFF0C\u8BA9\u7528\u6237\u5FEB\u901F\u8BC6\u522B\u800C\u975E\u56DE\u5FC6\uFF0C\u51CF\u5C11\u7528\u6237\u8BB0\u5FC6\u8D1F\u62C5\u3002</li></ul><h3>\u53EF\u63A7 Controllability</h3><ul><li><strong>\u7528\u6237\u51B3\u7B56\uFF1A</strong> \u6839\u636E\u573A\u666F\u53EF\u7ED9\u4E88\u7528\u6237\u64CD\u4F5C\u5EFA\u8BAE\u6216\u5B89\u5168\u63D0\u793A\uFF0C\u4F46\u4E0D\u80FD\u4EE3\u66FF\u7528\u6237\u8FDB\u884C\u51B3\u7B56\uFF1B</li><li><strong>\u7ED3\u679C\u53EF\u63A7\uFF1A</strong> \u7528\u6237\u53EF\u4EE5\u81EA\u7531\u7684\u8FDB\u884C\u64CD\u4F5C\uFF0C\u5305\u62EC\u64A4\u9500\u3001\u56DE\u9000\u548C\u7EC8\u6B62\u5F53\u524D\u64CD\u4F5C\u7B49\u3002</li></ul><hr><h1>\u5B89\u88C5</h1><p>\u5EFA\u8BAE\u60A8\u4F7F\u7528\u5305\u7BA1\u7406\u5668\uFF08\u5982 NPM\u3001Yarn \u6216 pnpm\uFF09\u5B89\u88C5 JwChat\uFF0C</p><p>\u4F7F\u7528<code>npm</code>\u5B89\u88C5\u3002</p><pre><code class="language-bash">npm install jwchat
</code></pre><p>\u4F7F\u7528<code>yarn</code>\u5B89\u88C5\u3002</p><pre><code class="language-bash">yarn add jwchat
</code></pre><p>\u4F7F\u7528<code>pnpm</code>\u5B89\u88C5\u3002</p><pre><code class="language-bash">pnpm install jwchat
</code></pre><p>\u5982\u679C\u60A8\u7684\u7F51\u7EDC\u73AF\u5883\u4E0D\u597D\uFF0C\u5EFA\u8BAE\u4F7F\u7528\u76F8\u5173\u955C\u50CF\u670D\u52A1 cnpm \u6216 \u4E2D\u56FD NPM \u955C\u50CF\u3002</p><hr><h1>\u4F7F\u7528</h1><ol><li>\u5728 <code>main.js</code> \u4E2D\u5F15\u5165\u7EC4\u4EF6</li></ol><pre><code class="language-js">import JwChat from &quot;jwchat&quot;;
import &quot;jwchat/lib/style.css&quot;;

app.use(JwChat);
</code></pre><ol start="2"><li>\u5728 <code>\\*.vue</code> \u4E2D\u5F15\u5165</li></ol><pre><code class="language-vue">&lt;JwChat
  :taleList=&quot;list&quot;
  @enter=&quot;bindEnter&quot;
  v-model=&quot;inputMsg&quot;
  :showRightBox=&quot;true&quot;
  scrollType=&quot;noscroll&quot;
/&gt;
</code></pre><hr><h1>\u4EE5\u6587\u4EF6\u7684\u5F62\u5F0F\u5728\u672C\u5730\u76F4\u63A5\u5F15\u5165</h1><p><em>\u8BF7\u4E0D\u8981\u653E\u5728<code>public</code> \u6587\u4EF6\u5939\u4E2D\u3002<code>import\`\`public</code> \u6587\u4EF6\u5939\u4E2D\u7684\u6587\u4EF6\u4E2D\u7684 js\uFF0C\u5728\u6253\u5305\u65F6\u5019\u4F1A\u62A5\u9519</em></p><ol><li><p>\u5C06<code>lib</code>\u4EE3\u7801\u653E\u5728<code>/src/assets</code>\u6587\u4EF6\u5939\u4E2D</p></li><li><p>\u5728 <code>main.ts</code> \u4E2D\u5F15\u5165\u7EC4\u4EF6</p></li></ol><pre><code class="language-js">import JwChat from &quot;./assets/lib/JwChat.es.js&quot;;
import &quot;./assets/lib/style.css&quot;;

createApp(App).use(JwChat).mount(&quot;#app&quot;);
</code></pre><ol start="3"><li>\u5728 <code>\\*.vue</code> \u4E2D\u5F15\u5165</li></ol><pre><code class="language-vue">&lt;JwChat
  :taleList=&quot;list&quot;
  @enter=&quot;bindEnter&quot;
  v-model=&quot;inputMsg&quot;
  :showRightBox=&quot;true&quot;
  scrollType=&quot;noscroll&quot;
/&gt;
</code></pre><ol start="4"><li>\u5982\u679C\u9879\u76EE\u4F7F\u7528\u7684\u662F<code>typescript</code> \u9700\u8981\u5728<code>src</code>\u76EE\u5F55\u4E0B<code>*.d.ts</code>\u4E2D\u58F0\u660E\u7C7B\u578B\u3002</li></ol><pre><code>// env.d.ts \u6216\u8005 vite-env.d.ts
declare module &quot;*/JwChat.es.js&quot;;
</code></pre>`,31),r={__name:"index",setup(p,{expose:e}){return e({frontmatter:{}}),(d,u)=>(c(),t("div",l,[s(`
 * @Author       : Bian <389701057@qq.com>
 * @Date         : 2022-10-20 23:42:28
 * @LastEditors  : Bian <389701057@qq.com>
 * @LastEditTime : 2022-10-20 23:54:34
 * @FilePath     : \\src\\pages\\index.md
 * @Description  : \u4E3B\u9875
 * Copyright (c) 2022 by Bian <389701057@qq.com>, All Rights Reserved.
`),a]))}},m=o(r,[["__file","/Users/wlm/code/chat/src/pages/index.md"]]);export{m as default};
