import{_ as h,f as s,o as c,c as g,g as n,w as d,a5 as p,h as a,L as q,a as i,b as w}from"./index.d5e3151d.js";const f={data(){return{show:!1,rightConfig:{}}},watch:{show:{handler(t){let e={listTip:"\u5F53\u524D\u5728\u7EBF",list:[{name:"JwChat",img:"../image/three.jpeg"},{name:"\u7559\u604B\u4EBA\u95F4\u4E0D\u7FA1\u4ED9",img:"../image/one.jpeg"},{name:"\u53EA\u76FC\u6D41\u661F\u4E0D\u76FC\u96E8",img:"../image/two.jpeg"},{name:"JwChat",img:"../image/three.jpeg"},{name:"\u7559\u604B\u4EBA\u95F4\u4E0D\u7FA1\u4ED9",img:"../image/one.jpeg"},{name:"\u53EA\u76FC\u6D41\u661F\u4E0D\u76FC\u96E8",img:"../image/two.jpeg"},{name:"JwChat",img:"../image/three.jpeg"},{name:"\u7559\u604B\u4EBA\u95F4\u4E0D\u7FA1\u4ED9",img:"../image/one.jpeg"},{name:"\u53EA\u76FC\u6D41\u661F\u4E0D\u76FC\u96E8",img:"../image/two.jpeg"},{name:"JwChat",img:"../image/three.jpeg"},{name:"\u7559\u604B\u4EBA\u95F4\u4E0D\u7FA1\u4ED9",img:"../image/one.jpeg"},{name:"\u53EA\u76FC\u6D41\u661F\u4E0D\u76FC\u96E8",img:"../image/two.jpeg"}]};t&&(e.notice=`\u3010\u516C\u544A\u3011\u8FD9\u662F\u4E00\u6B3E\u9AD8\u5EA6\u81EA\u7531\u7684\u804A\u5929\u7EC4\u4EF6\uFF0C\u57FA\u4E8E
            AVue\u3001Vue\u3001Element-ui\u5F00\u53D1\u3002\u70B9\u4E2A\u8D5E\u518D\u8D70\u5427 `),this.rightConfig=e},immediate:!0}},methods:{rightClick(t){console.log("rigth",t)},showNotice(){this.show=!this.show}}};function C(t,e,u,r,m,o){const l=s("el-button"),_=s("JwChat-rightbox");return c(),g(q,null,[n(l,{onClick:o.showNotice,size:"small",style:{"margin-top":"1rem"}},{default:d(()=>[p(" \u4F20\u5165\u516C\u544A ")]),_:1},8,["onClick"]),a("div",null,[n(_,{class:"rightSlot",config:m.rightConfig,onClick:o.rightClick},null,8,["config","onClick"])])],64)}const j=h(f,[["render",C],["__scopeId","data-v-63a478d9"],["__file","/Users/wlm/code/chat/packages/RightList/docs/demo.vue"]]),b={class:"markdown-body"},k=a("h1",null,"\u53F3\u4FA7\u7EC4\u4EF6",-1),x=a("p",null,"\u9ED8\u8BA4\uFF1A\u8FD9\u662F\u4E00\u4E2A\u65B0\u7EC4\u4EF6",-1),y=a("h2",null,"\u57FA\u7840\u7528\u6CD5",-1),v=w(`<h2>\u53C2\u6570\u914D\u7F6E</h2><hr><h3>Attribute</h3><table><thead><tr><th>\u53C2\u6570</th><th style="text-align:left;">\u8BF4\u660E</th><th>\u7C7B\u578B</th><th>\u53EF\u9009\u503C</th><th>\u9ED8\u8BA4\u503C</th></tr></thead><tbody><tr><td>config</td><td style="text-align:left;">\u8F93\u5165\u6846\u4E2D\u7684\u6587\u5B57</td><td>Object</td><td>-</td><td>-</td></tr></tbody></table><h3>Methods</h3><table><thead><tr><th>\u53C2\u6570</th><th>\u8BF4\u660E</th><th>\u53C2\u6570</th></tr></thead><tbody><tr><td>click</td><td>\u70B9\u51FB\u5217\u8868\u89E6\u53D1\u56DE\u8C03\u51FD\u6570</td><td>\u9009\u4E2D\u7684\u8282\u70B9</td></tr></tbody></table><h4><code>config</code>\u53C2\u6570</h4><pre><code class="language-json">// \u683C\u5F0F
{
  &quot;tip&quot;: &quot;\u7FA4\u516C\u544A&quot;,
  &quot;notice&quot;: &quot;\u3010\u516C\u544A\u3011\u8FD9\u662F\u4E00\u6B3E\u9AD8\u5EA6\u81EA\u7531\u7684\u804A\u5929\u7EC4\u4EF6\uFF0C\u57FA\u4E8EAVue\u3001Vue\u3001Element-ui\u5F00\u53D1\u3002\u70B9\u4E2A\u8D5E\u518D\u8D70\u5427 &quot;,
  &quot;listTip&quot;: &quot;\u5F53\u524D\u5728\u7EBF&quot;,
  &quot;list&quot;: [
    {
      &quot;name&quot;: &quot;JwChat&quot;,
      &quot;img&quot;: &quot;../image/three.jpeg&quot;
    },
    {
      &quot;name&quot;: &quot;\u7559\u604B\u4EBA\u95F4\u4E0D\u7FA1\u4ED9&quot;,
      &quot;img&quot;: &quot;../image/one.jpeg&quot;
    },
    {
      &quot;name&quot;: &quot;\u53EA\u76FC\u6D41\u661F\u4E0D\u76FC\u96E8&quot;,
      &quot;img&quot;: &quot;../image/two.jpeg&quot;
    }
  ]
}
</code></pre>`,8),E={__name:"README",setup(t,{expose:e}){return e({frontmatter:{}}),(r,m)=>{const o=s("Preview");return c(),g("div",b,[i(" \u52A0\u8F7D demo \u7EC4\u4EF6 start "),i(" \u52A0\u8F7D demo \u7EC4\u4EF6 end "),i(" \u6B63\u6587\u5F00\u59CB "),k,x,y,n(o,{"comp-name":"RightList","demo-name":"demo"},{default:d(()=>[n(j)]),_:1}),v])}}},J=h(E,[["__file","/Users/wlm/code/chat/packages/RightList/docs/README.md"]]);export{J as default};
